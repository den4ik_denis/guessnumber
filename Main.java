package com.GuessNumber;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Hi, I am guessing a number from min to max of your range. Try to guess it in X tries!");
        System.out.println("-----------------------------------------------------------------------------------------");
        Start.startGame(sc);
        AnswerUser answerUser = new AnswerUser();
        answerUser.getAnswer(sc);
        sc.close();
    }
}
