package com.GuessNumber;


public class Number {

    private int hiddenNum;
    private int clientNum;
    private int attemptNum;

    public int getHiddenNum() {

        return this.hiddenNum;
    }

    public void setHiddenNum(int hiddenNum) {

        if (hiddenNum > 0 && hiddenNum <= 200) {

            this.hiddenNum = hiddenNum;
        } else {
            System.out.println("Range hidden number only integers 1 - 200");
            System.out.println("-----------------------------------------------------------------------------------------");
        }

    }

    public int getClientNum() {
        return clientNum;
    }

    public void setClientNum(int clientNum) {
        if (clientNum >= 0 && clientNum <= 200) {
            this.clientNum = clientNum;
        } else {
            System.out.println("Range number only integers 1 - 200");
            System.out.println("-----------------------------------------------------------------------------------------");
        }
    }

    public int getAttemptNum() {
        return attemptNum;
    }

    public void setAttemptNum(int attemptNum) {
        if (attemptNum > 0 && attemptNum <= 15) {
            this.attemptNum = attemptNum;
        } else {
            System.out.println("Range attempt numbers 1-15");
            System.out.println("-----------------------------------------------------------------------------------------");
        }
    }

    public Number(int clientNum, int hiddenNum, int attemptNum) {
        this.clientNum = clientNum;
        this.hiddenNum = hiddenNum;
        this.attemptNum = attemptNum;
    }
}
