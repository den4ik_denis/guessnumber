package com.GuessNumber;

import java.util.Scanner;


public class Setting {


    public static int makeAttempt() {

        System.out.println("How many attempts you want? min 1 and max 15!");
        System.out.println("-----------------------------------------------------------------------------------------");
        Scanner sc = new Scanner(System.in);

        if (sc.hasNextInt()) {
            int a = sc.nextInt();
            if (a > 1 && a < 15)
                return a;
        } else {
            System.out.println("Only numbers 1-15 !!");
            makeAttempt();
        }
        System.out.println("Rage attempt 1-15  !!");
        return makeAttempt();
    }
}
