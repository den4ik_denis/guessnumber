package com.GuessNumber;

import java.util.Scanner;

public class AnswerUser {

    Number number = new Number(0, RandomNum.randomNum(), Setting.makeAttempt());

    int count = number.getAttemptNum();

    public void getAnswer(Scanner sc) {


        System.out.println("Try to guess it if you want EXIT write 0");

        while (count > 0) {

            if (sc.hasNextInt()) {

                number.setClientNum(sc.nextInt());

                int client = number.getClientNum();

                int randomNumber = number.getHiddenNum();


                if (client > randomNumber) {
                    count--;
                    System.out.println("Random number is less than yours ");
                    System.out.println("attempts left " + count + " attempt");
                    System.out.println("-----------------------------------------------------------------------------------------");

                } else if (client == randomNumber) {

                    System.out.println("Congratulations! You guessed the planned number for - " + (number.getAttemptNum() - count + 1) + " attempts");
                    System.exit(0);

                } else if (client == 0) {
                    System.exit(0);
                } else {
                    count--;
                    System.out.println("Random number is greater than yours");
                    System.out.println("attempts left " + count + " attempt");
                    System.out.println("-----------------------------------------------------------------------------------------");
                }
            } else {
                System.out.println("Write plz only NUMBERS!");
                System.out.println("-----------------------------------------------------------------------------------------");
                break;
            }
        }
        System.exit(0);
    }
}