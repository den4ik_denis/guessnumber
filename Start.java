package com.GuessNumber;

import java.util.Scanner;

public class Start {

    private static int tries = 5;
    private static final String LESS_NUMBER = "My number is less than yours ";
    private static final String GREATER_NUMBER = "My number is greater than yours";
    private static final String GUESSED_NUMBER = "Wow you guessed the number for";
    private static final String RAGE_NUMBERS = "Only number rage 1-100";
    private static final String ONLY_NUMBERS = "Only numbers!";

    public static void startGame(Scanner scanner) {

        int randomNumber = (int) (Math.random() * 101);
        System.out.println("Guess a number from 1 to 100 in 5 tries");
        System.out.println("-----------------------------------------------------------------------------------------");
        while (tries > 0) {

            if (scanner.hasNextInt()) {

                int cNumber = scanner.nextInt();


                if (cNumber > 0 && cNumber <= 100) {
                    if (cNumber > randomNumber) {
                        tries--;
                        System.out.println(LESS_NUMBER);
                        System.out.println("attempts left " + tries);
                        System.out.println("-----------------------------------------------------------------------------------------");
                    } else if (cNumber < randomNumber) {
                        tries--;
                        System.out.println(GREATER_NUMBER);
                        System.out.println("attempts left " + tries);
                        System.out.println("-----------------------------------------------------------------------------------------");
                    } else {
                        System.out.println(GUESSED_NUMBER);
                        System.out.println("-----------------------------------------------------------------------------------------");
                    }
                } else {
                    System.out.println(RAGE_NUMBERS);
                    System.out.println("-----------------------------------------------------------------------------------------");
                }
            } else {
                System.out.println(ONLY_NUMBERS);
                System.out.println("-----------------------------------------------------------------------------------------");
            }
        }
    }
}