package com.GuessNumber;

import java.util.Scanner;

public class RandomNum {

    public static int randomNum() {

        Scanner sc = new Scanner(System.in);

        int result;

        System.out.println("Write plz range min and max (1-200)");
        System.out.println("-----------------------------------------------------------------------------------------");


        if (sc.hasNextInt()) {

            int min = sc.nextInt();
            int max = sc.nextInt();

            if (min > 0 && min < max && max <= 200) {

                result = (min + (int) (Math.random() * ((max - min) + 1)));

                return result;
            } else if (min > 0 && min > max && min <= 200) {

                result = (max + (int) (Math.random() * ((min - max) + 1)));

                return result;
            } else {
                randomNum();
            }
        }
        return -1;
    }
}